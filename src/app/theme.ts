'use client'
import { ThemeOptions, createTheme } from '@mui/material/styles'
import { Roboto } from 'next/font/google'

const font = Roboto({ subsets: ['latin'], weight: '400' })

const themeOptions: ThemeOptions = {
  palette: {
    primary: {
      main: '#9c27b0',
    },
  },
  typography: {
    fontFamily: font.style.fontFamily,
  },
}

export const theme: ThemeOptions = createTheme(themeOptions)
